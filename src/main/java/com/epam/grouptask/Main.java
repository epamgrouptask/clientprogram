package com.epam.grouptask;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import com.epam.grouptask.fx.Controller;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application {
	private boolean firstTime;
	private TrayIcon trayIcon;

	@Override
	public void start(Stage primaryStage) throws Exception {
		createTrayIcon(primaryStage);
        firstTime = true;
        Platform.setImplicitExit(false);
        
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
		Parent root = loader.load();
		Controller controller = (Controller) loader.getController();
		controller.setStage(primaryStage);
		primaryStage.setTitle("Clouds Merger");
		primaryStage.setScene(new Scene(root, 990, 640));
		primaryStage.show();
	}
	
	public void createTrayIcon(final Stage stage) {
        if (SystemTray.isSupported()) {
            // get the SystemTray instance
            SystemTray tray = SystemTray.getSystemTray();
            // load an image
            Image image = null;
            try {
                URL url = new URL("http://icons.iconarchive.com/icons/graphicloads/100-flat-2/16/cloud-icon.png");
                //File url = new File("E:\\JavaLab\\projects\\GroupTaskClient\\Cloud.ico");
                image = ImageIO.read(url);
                stage.getIcons().add(new javafx.scene.image.Image("http://icons.iconarchive.com/icons/graphicloads/100-flat-2/128/cloud-icon.png"));
            } catch (IOException ex) {
                System.out.println(ex);
            }

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    hide(stage);
                }
            });
            // create a action listener to listen for default action executed on the tray icon
            final ActionListener closeListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            };

            ActionListener showListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            stage.show();
                        }
                    });
                }
            };
            // create a popup menu
            PopupMenu popup = new PopupMenu();

            MenuItem showItem = new MenuItem("Show");
            showItem.addActionListener(showListener);
            popup.add(showItem);

            MenuItem closeItem = new MenuItem("Close");
            closeItem.addActionListener(closeListener);
            popup.add(closeItem);

            // construct a TrayIcon
            trayIcon = new TrayIcon(image, "Cloud merger", popup);
            // set the TrayIcon properties
            trayIcon.addActionListener(showListener);

            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println(e);
            }
        }
    }

    private void hide(final Stage stage) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (SystemTray.isSupported()) {
                    stage.hide();
                    //showProgramIsMinimizedMsg();
                } else {
                    System.exit(0);
                }
            }
        });
    }

	public static void main(String[] args) {
		launch(args);
	}
}
