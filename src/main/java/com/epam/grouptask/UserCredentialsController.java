package com.epam.grouptask;

/**
 * Created by Andrian on 30.01.2016.
 */

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;

import java.io.*;
import java.util.Properties;

public class UserCredentialsController {
    private static StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    static {
        encryptor.setPassword("groupcloudmerger");
    }

    public static void set(String username, String password) throws IOException {
        Properties props = new EncryptableProperties(encryptor);
        InputStream is = new FileInputStream("src\\main\\resources\\credentials.properties");
        props.load(is);
        is.close();
        OutputStream os = new FileOutputStream("src\\main\\resources\\credentials.properties");
        props.setProperty("username", encryptor.encrypt(username));
        props.setProperty("password", encryptor.encrypt(password));
        props.store(os, null);
        os.close();
    }

    public static void get(StringBuffer username, StringBuffer pw) throws IOException {
        Properties props = new EncryptableProperties(encryptor);
        InputStream is = new FileInputStream("src\\main\\resources\\credentials.properties");
        props.load(is);
        is.close();
        String datasourceUsername = props.getProperty("username");
        String datasourcePassword = props.getProperty("password");
        username.append(encryptor.decrypt(datasourceUsername));
        pw.append(encryptor.decrypt(datasourcePassword));
    }

    public static void clear() throws IOException {
    	PrintWriter writer = new PrintWriter("src\\main\\resources\\credentials.properties");
    	writer.print("");
    	writer.close();

    }
}
