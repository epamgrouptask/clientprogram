package com.epam.grouptask;

import com.epam.grouptask.exception.ClientSettingsException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.server.client.SocketClient;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrian on 29.01.2016.
 */
public class ClientProgramController {
    private static ClientProgram clientProgram;
    private static ExecutorService es = Executors.newSingleThreadExecutor();

    public static void setExecutor(){
        es.execute(new SocketClient());
    }

    public static void stopExecutor(){
        es.shutdownNow();
        try {
            es.awaitTermination(1, TimeUnit.MICROSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static ClientProgram getInstance() throws ClientSettingsException {
        if (clientProgram == null) {
            try (ObjectInput is = new ObjectInputStream(new FileInputStream("client.cl"))) {
                clientProgram = (ClientProgram) is.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new ClientSettingsException();
            }
        }
        return clientProgram;
    }

    public static void setClientProgram(ClientProgram cp) {
        clientProgram = cp;
        save();
    }

    public static void save() {
        if (clientProgram != null) {
            try (ObjectOutput os = new ObjectOutputStream(new FileOutputStream("client.cl"))) {
                os.writeObject(clientProgram);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getHostName() {
        String hostname = "Unknown";
        try {
            InetAddress addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        } catch (UnknownHostException ex) {
            System.out.println("Hostname can not be resolved");
        }
        return hostname;
    }

    public static String getMAC() {
        InetAddress ip;
        String macAddr = "";
        try {
            ip = InetAddress.getLocalHost();
            System.out.println("Current IP address : " + ip.getHostAddress());
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            byte[] mac = network.getHardwareAddress();
            System.out.print("Current MAC address : ");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X", mac[i]/*, (i < mac.length - 1) ? "-" : ""*/));
            }
            System.out.println(sb.toString());
            macAddr = sb.toString();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return macAddr;
    }

    public static int login(String un, String pw, WebView webView) throws IOException {
        WebEngine webEngine = webView.getEngine();
        String url = getServAddress() + "/client/login";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        String urlParameters = "username=" + un + "&password=" + pw;

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return Integer.parseInt(response.toString());
    }

    public static String getClientId(String id, WebView webView) throws IOException {
        WebEngine webEngine = webView.getEngine();
        String url = getServAddress() + "/client/reg";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        //con.setRequestProperty("User-Agent", USER_AGENT);
        //con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String urlParameters = "name=" + getHostName() + "&id=" + id + "&mac=" + getMAC();

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public static int socialLogin(String url, WebView webView) {
        Pattern p = Pattern.compile("user_id=");
        Matcher matcher = p.matcher(url);
        String snId = null;
        int id = 0;
        if (matcher.find()) {
            int end = matcher.end();
            snId = url.substring(end);
            String social = "google";
            if (url.contains("vk")) {
                social = "vk";
            }
            try {
                id = loginUser(snId, social, webView);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    public static int loginUser(String id, String social, WebView webView) throws IOException {
        WebEngine webEngine = webView.getEngine();
        String url = getServAddress() + "/client/social";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        String urlParameters = "social=" + social + "&id=" + id;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return Integer.valueOf(String.valueOf(response));
    }

    public static List<String> getPathsFromServer(String clientId) throws IOException, JSONException {
        String url = getServAddress() + "/client/userpaths";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        String urlParameters = "clientId=" + clientId;

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'post' request to URL : " + url);
        System.out.println("Get parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        JSONArray jsonArray = new JSONArray(response.toString());
        List<String> paths = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            paths.add(jsonArray.getString(i));
        }
        return paths;
    }

    public static void changePath(List<String> path, String userId, String clientId, WebView webView) throws IOException {
        WebEngine webEngine = webView.getEngine();
        String url = getServAddress() + "/client/paths";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        String paths = String.join(",", path);
        String urlParameters = "userId=" + userId + "&clientId=" + clientId + "&paths=" + paths;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
    }

    public static void removePath(String path, String clientId, WebView webView) throws IOException {
        WebEngine webEngine = webView.getEngine();
        String url = getServAddress() + "/client/paths";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("PUT");
        String urlParameters = "clientId=" + clientId + "&path=" + path;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'PUT' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
    }

    public static String getServAddress() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = ClientProgramController.class.getClassLoader().getResourceAsStream("serv.properties");
            properties.load(inputStream);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("address");
    }

    private ClientProgramController() {
    }
}
