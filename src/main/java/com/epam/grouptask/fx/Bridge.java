package com.epam.grouptask.fx;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;

import com.epam.grouptask.ClientProgramController;
import com.epam.grouptask.UserCredentialsController;
import com.epam.grouptask.exception.ClientSettingsException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.server.client.SocketClient;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * Created by Andrian on 27.01.2016.
 */
public class Bridge {

	private WebView webView;
	private Stage stage;

	public Bridge(WebView webView, Stage stage) {
		this.webView = webView;
		this.stage = stage;
	}

	public void exit() {
		Platform.exit();
	}

	public void removePath(String path) {
		try {
			ClientProgram clientProgram = ClientProgramController.getInstance();
			List<String> paths = clientProgram.getPaths();
			for (int i = 0; i < paths.size(); i++) {
				if (paths.get(i).equals(path)) {
					paths.remove(i);
				}
			}
		} catch (ClientSettingsException e) {
			e.printStackTrace();
		}
	}

	public void loginGoogle(String userIdGoogle) {
		int userId = 0;
		try {
			userId = ClientProgramController.loginUser(userIdGoogle, "google", webView);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (userId != 0) {
			ClientProgram clientProgram = null;
			try {
				clientProgram = ClientProgramController.getInstance(); // get
																		// client
																		// program
				// redirect to client home|profile
			} catch (ClientSettingsException e) {
				String clientId = null;
				try {
					clientId = ClientProgramController.getClientId(String.valueOf(userId), webView);
				} catch (IOException e1) {
					e.printStackTrace();
				}
				if (clientId != null) {
					clientProgram = new ClientProgram();
					clientProgram.setMac(ClientProgramController.getMAC());
					clientProgram.setName(ClientProgramController.getHostName());
					clientProgram.setClientId(clientId);
					clientProgram.setUserId(userId);
					ClientProgramController.setClientProgram(clientProgram);

					// forward to tree
				}
			}
		}
	}

	public void sendId(String userId, String login, String pass) {
		WebEngine engine = webView.getEngine();
		if (userId != null) { // if we successfully loged in
			ClientProgram clientProgram = null;

			try {
				UserCredentialsController.set(login, pass);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				clientProgram = ClientProgramController.getInstance();

				engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId="
						+ clientProgram.getClientId() + "&userId=" + clientProgram.getUserId());

				// we have parameters and can work
				// redirect to profile
			} catch (ClientSettingsException e) { // if we doesn't have clientId
				// saved in file
				String clientId = null;
				List<String> paths = new ArrayList<>();
				try {
					clientId = ClientProgramController.getClientId(userId, webView);
					paths = ClientProgramController.getPathsFromServer(clientId);
				} catch (IOException e1) {
					e.printStackTrace();
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				clientProgram = new ClientProgram();
				clientProgram.setUserId(Integer.parseInt(userId));
				clientProgram.setClientId(clientId);
				clientProgram.setMac(ClientProgramController.getMAC());
				clientProgram.setName(ClientProgramController.getHostName());
				clientProgram.setPaths(paths);
				ClientProgramController.setClientProgram(clientProgram);

				// redirect to tree
				engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId="
						+ clientProgram.getClientId() + "&userId=" + clientProgram.getUserId() + "&tab=" + "paths");
			}
			ExecutorService es = Executors.newFixedThreadPool(1);
			es.execute(new SocketClient());
		}
	}

	public void changeLocale(String locale) {
		WebEngine engine = webView.getEngine();
		ClientProgram clientProgram = null;
		try {
			clientProgram = ClientProgramController.getInstance();
			engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId="
					+ clientProgram.getClientId() + "&userId=" + clientProgram.getUserId() + "&language=" + locale); // load
																														// profile
		} catch (ClientSettingsException e) {
			e.printStackTrace();
		}
	}

	public void changeName(String newName){
		try {
			ClientProgram client = ClientProgramController.getInstance();
            client.setName(newName);
            ClientProgramController.setClientProgram(client);
		} catch (ClientSettingsException e) {
			e.printStackTrace();
		}
	}

	public void changePath(String userId, String paths, String id, int added) {
		WebEngine engine = webView.getEngine();
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("JavaFX Projects");
		File defaultDirectory = new File("C:\\");
		List<String> pathsToRemove = null;
		chooser.setInitialDirectory(defaultDirectory);
		File selectedDirectory = chooser.showDialog(stage); // dialog with user
		String clientId = null;
		try {
			clientId = ClientProgramController.getInstance().getClientId();
		} catch (ClientSettingsException e) {
			e.printStackTrace();
		}
		if (selectedDirectory == null) {
			engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientId + "&userId="
					+ userId + "&tab=" + "paths");
			return;
		}
		List<String> pathes = new ArrayList<String>();
		for (String str : paths.split(",")) {
			if (!"".equals(str)) {
				pathes.add(str);
			}
		}
		Iterator<String> iterator = pathes.iterator();
		while(iterator.hasNext()){
			String path = iterator.next();
		//for (String path : pathes) {
			// 1) if new path is higher in hierarchy
			// 2) if new path is lower in hierarchy
			// 3) if new path is something else
			if (!selectedDirectory.getPath().contains(path) && path.contains(selectedDirectory.getPath())) {
				try {
					ClientProgramController.removePath(path, clientId, webView);
				} catch (IOException e) {
					e.printStackTrace();
				}
				iterator.remove();				
				continue;				
			}
			if (!id.equals(path)) {
				String slash = String.valueOf("\\\\");
				String twoSlash = String.valueOf("/");
				path = path.replaceAll(slash, twoSlash);
				Pattern pattern = Pattern.compile("^" + path + ".*"); // 2)
				String path1 = selectedDirectory.getAbsolutePath().replaceAll("\\\\", "/");
				Matcher matcher = pattern.matcher(path1);
				if (matcher.matches()) {
					try {
						ClientProgramController.removePath(id, clientId, webView);
					} catch (IOException e) {
						e.printStackTrace();
					}
					engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientId
							+ "&userId=" + userId + "&tab=" + "paths");
					return;
				}
			}
		}
		if (added != 1 || pathes.contains(id)) {
			try {
				ClientProgramController.removePath(id, clientId, webView);
			} catch (IOException e) {
				e.printStackTrace();
			}
			pathes.remove(id); // id - index of old path
		}
		pathes.add(selectedDirectory.getAbsolutePath());

		try {
			ClientProgramController.changePath(pathes, userId, clientId, webView);
			ClientProgram cp = ClientProgramController.getInstance();
			cp.setPaths(pathes);
			ClientProgramController.setClientProgram(cp);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClientSettingsException e) {
			e.printStackTrace();
		}
		engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientId + "&userId="
				+ userId + "&tab=" + "paths");
	}

	public void logout() throws IOException {
		WebEngine engine = webView.getEngine();
		try {
			UserCredentialsController.clear();
			PrintWriter writer = new PrintWriter("client.cl");
			writer.print("");
			writer.close();
			ClientProgramController.setClientProgram(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		engine.load(ClientProgramController.getServAddress() + "/client/logout");
		stage.close();
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
		Parent root = loader.load();
		Controller controller = (Controller) loader.getController();
		controller.setStage(stage);
		stage.setTitle("Clouds Merger");
		stage.setScene(new Scene(root, 990, 640));
		stage.show();
		ClientProgramController.setClientProgram(null);
		ClientProgramController.stopExecutor();
	}
}