package com.epam.grouptask.fx;

import com.epam.grouptask.ClientProgramController;
import com.epam.grouptask.UserCredentialsController;
import com.epam.grouptask.exception.ClientSettingsException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.server.client.SocketClient;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Controller {
    @FXML
    WebView webViewClient;
    Stage stage;
    @FXML
    javafx.scene.control.MenuItem exit;

    private boolean isLogined = false;
    private int userId;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        WebEngine engine = webViewClient.getEngine();

        StringBuffer un = new StringBuffer();
        StringBuffer pw = new StringBuffer();
        try {
            UserCredentialsController.get(un, pw);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!un.toString().equals("null") && !pw.toString().equals("null")) {
            ClientProgram clientProgram = null;
            try {
                int userId = ClientProgramController.login(un.toString(), pw.toString(), webViewClient);
                clientProgram = ClientProgramController.getInstance();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClientSettingsException e) {
                e.printStackTrace();
            } catch (NumberFormatException e){
                engine.load(ClientProgramController.getServAddress() + "/client/login");
                setListener(engine);
                return;
            }
             ClientProgramController.setExecutor();

            engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientProgram.getClientId() + "&userId="
                    + clientProgram.getUserId()); // load profile
        } else {
            engine.load(ClientProgramController.getServAddress() + "/client/login");
        }

//		JSObject windowObject = (JSObject) engine.executeScript("window");
//		windowObject.setMember("app", new Bridge(webViewClient, stage));

        setListener(engine);
    }

    public void setListener(WebEngine engine) {
        engine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
            @Override
            public void changed(ObservableValue ov, State oldState, State newState) {
                if (newState == State.SUCCEEDED) {
                    // listener
                    int userId = ClientProgramController.socialLogin(engine.getLocation(), webViewClient); // login
                    // with
                    StringBuffer un = new StringBuffer();
                    StringBuffer pw = new StringBuffer();
                    try {
                        UserCredentialsController.get(un, pw);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } // social
                    if (userId != 0) {

                        ClientProgram clientProgram = null;
                        try {
                            clientProgram = ClientProgramController.getInstance();

                            engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientProgram.getClientId() + "&userId="
                                    + clientProgram.getUserId()); // load profile

                            ExecutorService es = Executors.newFixedThreadPool(1);
                            es.execute(new SocketClient());

                            JSObject windowObject = (JSObject) engine.executeScript("window");
                            windowObject.setMember("app", new Bridge(webViewClient, stage));
                        } catch (ClientSettingsException e) {
                            String clientId = null;
                            List<String> paths = new ArrayList<>();
                            try {
                                clientId = ClientProgramController.getClientId(String.valueOf(userId), webViewClient);
                                paths = ClientProgramController.getPathsFromServer(clientId);
                            } catch (IOException e1) {
                                e.printStackTrace();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            if (clientId != null) {
                                clientProgram = new ClientProgram();
                                clientProgram.setMac(ClientProgramController.getMAC());
                                clientProgram.setName(ClientProgramController.getHostName());
                                clientProgram.setClientId(clientId);
                                clientProgram.setUserId(userId);
                                clientProgram.setPaths(paths);
                                ClientProgramController.setClientProgram(clientProgram);

                                engine.load(ClientProgramController.getServAddress() + "/client/paths?clientId=" + clientProgram.getClientId() + "&userId="
                                        + clientProgram.getUserId()); // load profile

                                JSObject windowObject = (JSObject) engine.executeScript("window");
                                windowObject.setMember("app", new Bridge(webViewClient, stage));
                            }
                        }
                    } /*else if (!engine.getLocation().equals(ClientProgramController.getServAddress() + "/client/login") && (un.toString().equals("null") || pw.toString().equals("null"))) {
                        engine.load(ClientProgramController.getServAddress() + "/client/login");
                    }*/
                    JSObject windowObject = (JSObject) engine.executeScript("window");
                    windowObject.setMember("app", new Bridge(webViewClient, stage));
                }
            }
        });

    }
}