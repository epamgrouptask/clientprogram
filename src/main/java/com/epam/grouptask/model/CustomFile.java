package com.epam.grouptask.model;

import java.io.Serializable;
import java.util.Date;

public class CustomFile implements Serializable{
    private static final long serialVersionUID = 1150169519310163575L;

    private Integer userId;
    private String name;
    private String path;
    private String type;
    private Long size;
    private Date lastChange;
    private String fileId;
    private String clientId;

    @Override
    public String toString() {
        return "CustomFile{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", type='" + type + '\'' +
                ", size=" + size +
                ", lastChange=" + lastChange +
                ", fileId='" + fileId + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}