package com.epam.grouptask.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrian on 24.01.2016.
 */
public class ClientProgram implements Serializable{
	private String clientId;
	private int userId;
	private String mac;
	private List<String> paths;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<String> getPaths() {
		return paths;
	}

	public void setPaths(List<String> paths) {
		this.paths = paths;
	}

	public void setPath(String path) {
		if (paths == null) {
			paths = new ArrayList<>();
		}
		paths.add(path);
	}

	public String getPath(int index) {
		if (paths != null) {
			return paths.get(index);
		}
		return null;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof ClientProgram))
			return false;

		ClientProgram that = (ClientProgram) o;

		if (getUserId() != that.getUserId())
			return false;
		return getClientId().equals(that.getClientId());

	}

	@Override
	public int hashCode() {
		int result = getClientId().hashCode();
		result = 31 * result + getUserId();
		return result;
	}
}
