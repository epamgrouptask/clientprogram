package com.epam.grouptask.exception;

/**
 * Created by Andrian on 30.01.2016.
 */
public class ClientSettingsException extends Exception {
    public ClientSettingsException() {
        super();
    }

    public ClientSettingsException(String message) {
        super(message);
    }

    public ClientSettingsException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientSettingsException(Throwable cause) {
        super(cause);
    }

    protected ClientSettingsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
