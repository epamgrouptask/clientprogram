package com.epam.grouptask.server.client;

import com.epam.grouptask.ClientProgramController;
import com.epam.grouptask.exception.ClientSettingsException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.protocol.Protocol;
import com.epam.grouptask.server.file.FileManager;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class SocketClient implements Runnable {
    public long TIMEOUT = 10000;
    public String ADDRESS = "127.0.0.1";
    public int PORT = 8511;

    private byte[] message = null;
    private Selector selector;

    {
        Properties properties = new Properties();
        try {
            //InputStream inputStream = ServerForFile.class.getClassLoader().getResourceAsStream("serv.properties");
            properties.load(getClass().getClassLoader().getResourceAsStream("serv.properties"));
            ADDRESS = properties.getProperty("asyncServerAddress");
            PORT = Integer.parseInt(properties.getProperty("asyncServerPort"));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public SocketClient() {
    }

    @Override
    public void run() {
        SocketChannel channel;
        try {
            selector = Selector.open();
            channel = SocketChannel.open();
            channel.configureBlocking(false);

            channel.register(selector, SelectionKey.OP_CONNECT);
            channel.connect(new InetSocketAddress(ADDRESS, PORT));

            while (!Thread.interrupted()) {

                selector.select(TIMEOUT);
                Iterator<SelectionKey> keys = selector.selectedKeys().iterator();

                while (keys.hasNext()) {
                    SelectionKey key = keys.next();
                    keys.remove();

                    if (!key.isValid()) continue;

                    if (key.isConnectable()) {
                        System.out.println("I am connected to the server");
                        connect(key);
                    }
                    if (key.isWritable()) {
                        write(key);
                    }
                    if (key.isReadable()) {
                        read(key);
                    }

                }
            }
        } catch (IOException e) {
            //System.out.println("Well-well");
            //System.exit(1);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    private void close() {
        try {
            selector.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer readBuffer = ByteBuffer.allocate(4);         //size of next message
        ByteBuffer messageBytes;
        readBuffer.clear();
        int readedBytes;
        int lengthOfMessage;
        try {
            readedBytes = channel.read(readBuffer);
            readBuffer.flip();
            lengthOfMessage = readBuffer.getInt();
            messageBytes = ByteBuffer.allocate(lengthOfMessage);
            messageBytes.clear();
            readedBytes = channel.read(messageBytes);
        } catch (IOException e) {
            System.out.println("Reading problem, closing connection");
            key.cancel();
            channel.close();
            return;
        }
        if (readedBytes == -1) {
            System.out.println("Nothing was read from server");
            channel.close();
            key.cancel();
            return;
        }
        messageBytes.flip();
        byte[] buff = new byte[lengthOfMessage];
        messageBytes.get(buff, 0, readedBytes);

        if ("ready".equals(new String(buff))) {
            Protocol protocol = new Protocol();
            try {
                protocol.setClientID(ClientProgramController.getInstance().getClientId());
            } catch (ClientSettingsException e) {
                e.printStackTrace();
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(protocol);
            out.flush();
            out.close();
            byte b[] = bos.toByteArray();

            bos.close();
            message = b;
            channel.register(selector, SelectionKey.OP_WRITE);
        } else {
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(buff);
                ObjectInput in = new ObjectInputStream(bis);
                Protocol protocol = (Protocol) in.readObject();
                switch (protocol.getTypeOfQuery()) {
                    //!!!ClientID to protocol
                    case "Get":     //directory -path
                        List<CustomFile> files = FileManager.getDirectory(protocol);
                        protocol.setDirectory(files);
                        protocol.setResponse(true);
                        break;
                    case "Delete": //-path
                        protocol.setBoolResp(FileManager.delete(protocol));
                        protocol.setResponse(true);
                            /*if (FileManager.delete(protocol)) {
                                List<File> fileList = FileManager.getDirectory(protocol);
                                //set fields in protocol
                            } else {
                                //response with error   -add to prototocol some field with errors
                            }*/
                        break;
                    case "Upload":     //upload file to client -path(name)
                        String p = protocol.getPath().split("/")[0];
                        if(FileUtils.getFile(protocol.getPath()).length() > 0 || "C:".equals(p)){
                            protocol.setBoolResp(false);
                        } else {
                            clientFileWorker(false, protocol.getPath());
                            protocol.setBoolResp(FileUtils.getFile(protocol.getPath()).length() > 0);
                        }
                        protocol.setResponse(true);
                        break;
                    case "Create":      //create directory -path
                        protocol.setBoolResp(FileManager.createDirectory(protocol));
                        protocol.setResponse(true);
                        break;
                    case "Rename":     //-path(oldName) -newPath (newName)
                        protocol.setBoolResp(FileManager.rename(protocol));
                        protocol.setResponse(true);
                        break;
                    case "Move":
                        protocol.setBoolResp(FileManager.move(protocol));
                        protocol.setResponse(true);
                        break;
                    case "Copy":        //path newPath
                        protocol.setBoolResp(FileManager.copy(protocol));
                        protocol.setResponse(true);
                        break;
                    case "Size":          //return free space and total space  !!!to protocol
                        long free = FileManager.getFreeSize();
                        protocol.setSize(free);
                        protocol.setBoolResp(true);
                        protocol.setResponse(true);
                        break;
                    case "Total":
                        long total = FileManager.getTotalSize();
                        protocol.setSize(total);
                        protocol.setBoolResp(true);
                        protocol.setResponse(true);
                        break;
                    case "Download":        //nameOfFile
                        clientFileWorker(true, protocol.getPath());
                        protocol.setBoolResp(true);
                        protocol.setResponse(true);
                        break;
                    case "Tree":
                        protocol.setTree(FileManager.getFileTree(protocol.getTree()));
                        protocol.setBoolResp(true);
                        protocol.setResponse(true);
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutput out = new ObjectOutputStream(bos);
                out.writeObject(protocol);
                byte b[] = bos.toByteArray();
                out.close();
                bos.close();
                message = b;
                channel.register(selector, SelectionKey.OP_WRITE);
                System.out.println(protocol);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (ClientSettingsException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void clientFileWorker(boolean download, String path) {
        File file = new File(path);
        ClientFile clientFile = new ClientFile(download, file);
        clientFile.start();
        try {
            clientFile.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        channel.write(ByteBuffer.wrap(dataWithItLength(message)));

        key.interestOps(SelectionKey.OP_READ);
    }

    private void connect(SelectionKey key) throws IOException, ClassNotFoundException {
        SocketChannel channel = (SocketChannel) key.channel();
        if (channel.isConnectionPending()) {
            channel.finishConnect();
        }
        channel.configureBlocking(false);
        channel.register(selector, SelectionKey.OP_READ);
    }

    private byte[] dataWithItLength(byte[] data) {
        byte[] newData = new byte[data.length + 4];
        System.arraycopy(ByteBuffer.allocate(4).putInt(data.length).array(),
                0, newData, 0, 4);
        System.arraycopy(data, 0, newData, 4, data.length);
        return newData;
    }
}
