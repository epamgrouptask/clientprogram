package com.epam.grouptask.server.client;

import com.epam.grouptask.ClientProgramController;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

/**
 * Created by 1 on 23.01.2016.
 */
public class ClientFile extends Thread {
    private static String serverName = "127.0.0.1";
    private static int port = 2222;

    static {
        Properties properties = new Properties();
        try {
            InputStream inputStream = ClientProgramController.class.getClassLoader().getResourceAsStream("serv.properties");
            properties.load(inputStream);
            serverName = properties.getProperty("asyncServerAddress");
            port = Integer.parseInt(properties.getProperty("fileServerPort"));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private int bufferSize = 16 * 1024;
    private File file;
    private boolean downloadToServer;

    ClientFile(boolean downloadToServer, File file) {
        this.downloadToServer = downloadToServer;
        this.file = file;
    }


    @Override
    public void run() {
        try {
            if (downloadToServer) {
                downloadFileToServer();
            } else {
                uploadFileToClient();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uploadFileToClient() throws IOException {
        System.out.println("Connecting to " + serverName +
                " on port " + port);
        Socket client = new Socket(serverName, port);
        System.out.println("Just connected to "
                + client.getRemoteSocketAddress());
        InputStream inFromServer = client.getInputStream();
        BufferedInputStream in = new BufferedInputStream(inFromServer, bufferSize);

        byte[] bytes = new byte[bufferSize];
        OutputStream out = new BufferedOutputStream(new FileOutputStream(file), bufferSize);
        int count;
        while ((count = in.read(bytes)) > 0) {
            out.write(bytes, 0, count);
            out.flush();
        }
        in.close();
        out.close();
        client.close();
    }

    private void downloadFileToServer() throws IOException {
        System.out.println("Connecting to " + serverName +
                " on port " + port);
        Socket client = new Socket(serverName, port);
        System.out.println("Just connected to "
                + client.getRemoteSocketAddress());

        OutputStream outToServer = client.getOutputStream();
        BufferedOutputStream out = new BufferedOutputStream(outToServer, bufferSize);

        byte[] bytes = new byte[bufferSize];
        InputStream is = new BufferedInputStream(new FileInputStream(file), bufferSize);
        int count;
        while ((count = is.read(bytes)) > 0) {
            out.write(bytes, 0, count);
            out.flush();
        }
        is.close();
        out.close();
        client.close();
    }
}
