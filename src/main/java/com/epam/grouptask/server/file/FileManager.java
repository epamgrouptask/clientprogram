package com.epam.grouptask.server.file;

import com.epam.grouptask.ClientProgramController;
import com.epam.grouptask.exception.ClientSettingsException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.protocol.Protocol;
import org.apache.commons.io.FileSystemUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrian on 23.01.2016.
 */
public class FileManager {

    public static boolean delete(Protocol protocol) {
        File file = FileUtils.getFile(protocol.getPath());

        try {
            if (file.isFile()) {
                FileUtils.forceDelete(file);
            } else if (file.isDirectory()) {
                FileUtils.deleteDirectory(file);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static List<CustomFile> getRootDirectory() throws ClientSettingsException {
        List<String> paths = ClientProgramController.getInstance().getPaths();

        List<CustomFile> customFiles = new ArrayList<>();
        for (String path : paths) {
            File directory = FileUtils.getFile(path);
            if (directory == null) {
                continue;
            }
            CustomFile customFile = new CustomFile();
            customFile.setName(directory.getName().equals("") ? directory.getPath() : directory.getName());
            customFile.setPath(directory.getPath());
            customFile.setType("folder");
            customFile.setFileId(ClientProgramController.getInstance().getName());
            customFile.setClientId(ClientProgramController.getInstance().getClientId());
            //customFile.setSize(directory.length());
            customFile.setLastChange(new Date(directory.lastModified()));
            customFiles.add(customFile);
            /*List<File> files = Arrays.asList(directory.listFiles());
            for (File file : files) {
                CustomFile customFile = new CustomFile();
                customFile.setName(file.getName());
                customFile.setPath(file.getPath());
                customFile.setType(file.isDirectory()? "folder" : "file");
                customFile.setSize(file.length());
                customFile.setLastChange(new Date(file.lastModified()));
                customFiles.add(customFile);
            }*/
        }
        return customFiles;
    }

    public static List<CustomFile> getDirectory(Protocol protocol) throws ClientSettingsException {
        String path = protocol.getPath();
        if ("".equals(path) || "/".equals(path)) {
            return getRootDirectory();
        }
        File directory = FileUtils.getFile(path);
        if (directory == null) {
            return null;
        }
        List<File> files = Arrays.asList(directory.listFiles());
        List<CustomFile> customFiles = new ArrayList<>();
        for (File file : files) {
            if (file.isHidden()) {
                continue;
            }
            CustomFile customFile = new CustomFile();
            customFile.setName(file.getName());
            customFile.setPath(file.getPath());
            customFile.setType(file.isDirectory() ? "folder" : "file");
            if(!file.isDirectory()) {
                customFile.setSize(file.length());
            }
            customFile.setLastChange(new Date(file.lastModified()));
            customFiles.add(customFile);
        }
        return customFiles;
    }

    public static boolean createDirectory(Protocol protocol) {
        boolean created = false;
        File directory = new File(protocol.getPath());
        if (!directory.exists()) {
            try {
                FileUtils.forceMkdir(directory);
                created = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            protocol.setError("Directory with this name already exist");
        }
        return created;
    }

    public static boolean rename(Protocol protocol) {
        boolean renamed = false;
        String oldPath = protocol.getPath();
        File oldName = new File(oldPath);
        String newPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
        File newName = new File(newPath + "/" + protocol.getNewpath());
        renamed = oldName.renameTo(newName);

        return renamed;
    }

    public static boolean move(Protocol protocol) {
        File sourceFile = FileUtils.getFile(protocol.getPath());
        File destination = FileUtils.getFile(protocol.getNewpath());
        boolean moved = false;
        try {
            if (sourceFile.isDirectory() && destination.isDirectory()) {
                FileUtils.moveDirectoryToDirectory(sourceFile, destination, true);
                moved = true;
            } else if (sourceFile.isFile() && destination.isDirectory()) {
                FileUtils.moveFileToDirectory(sourceFile, destination, true);
                moved = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return moved;
    }

    public static boolean copy(Protocol protocol) {
        File sourceFile = FileUtils.getFile(protocol.getPath());
        File destination = FileUtils.getFile(protocol.getNewpath());
        boolean copied = false;
        try {
            if (sourceFile.isDirectory() && destination.isDirectory()) {
                FileUtils.copyDirectoryToDirectory(sourceFile, destination);
                copied = true;
            } else if (sourceFile.isFile() && destination.isDirectory()) {
                FileUtils.copyFileToDirectory(sourceFile, destination);
                copied = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return copied;
    }

    public static long getFreeSize() throws ClientSettingsException {
        long freeSize = 0;
        List<String> paths = ClientProgramController.getInstance().getPaths();
        if(paths != null) {
            for (String path : paths) {
                try {
                    freeSize += FileSystemUtils.freeSpaceKb(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        /*try {
            freeSize = FileSystemUtils.freeSpaceKb("E:\\");
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        freeSize = freeSize * 1024;
        return freeSize;
    }

    public static long getTotalSize() throws ClientSettingsException {
        long fullSize = 0;
        List<String> paths = ClientProgramController.getInstance().getPaths();
        if(paths != null) {
            for (String path : paths) {
                File directory = FileUtils.getFile(path);
                fullSize += directory.getTotalSpace();
            }
        }
        return fullSize;
    }

    public static CustomDirectory getFileTree(CustomDirectory customDirectory) throws ClientSettingsException, JSONException {
        List<String> paths = ClientProgramController.getInstance().getPaths();
        if (paths == null) return customDirectory;
        //JSONObject jsonObject = new JSONObject(customDirectory);
        //CustomDirectory directory = new CustomDirectory(jsonObject.getString("text"), jsonObject.getString("path"));
        List<CustomDirectory> rootDirectories = new ArrayList<>();
        for (String path : paths) {
            CustomDirectory customDirectory1 = new CustomDirectory(path, path);
            getTree(customDirectory1, 0);
            rootDirectories.add(customDirectory1);
        }
        customDirectory.setInnerDir(rootDirectories);
        //directory.setInnerDir(rootDirectories);
        return customDirectory;
        //return directory.nodesToJson();
    }

    private static void getTree(CustomDirectory root, int lvl) {
        File directory = FileUtils.getFile(root.getPath());
        if (directory == null || lvl > 1) {
            return;
        }
        if(directory.listFiles() == null){
            return;
        }
        List<File> allFiles = Arrays.asList(directory.listFiles());
        for (File file : allFiles) {
            if (!file.isDirectory() || file.isHidden()) {
                continue;
            }
            CustomDirectory customDirectory = new CustomDirectory(file.getName(), file.getPath());
            getTree(customDirectory, lvl + 1);
            root.addDirectory(customDirectory);
        }
    }
}